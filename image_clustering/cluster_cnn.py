import keras.backend as K
from keras.engine.topology import Layer, InputSpec
from keras.layers import Dense, Input
from keras.optimizers import SGD
from sklearn.cluster import KMeans

from keras import layers
from keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D, Flatten, Reshape, Conv2DTranspose
from keras.models import Model
import numpy as np
import keras
import tensorflow as tf
from sklearn.metrics import normalized_mutual_info_score, adjusted_rand_score, davies_bouldin_score, silhouette_score

np.random.seed(0) 
tf.set_random_seed(0)

def target_distribution(q):
    weight = q ** 2 / q.sum(0)
    return (weight.T / weight.sum(1)).T 

class ClusteringLayer(Layer):
    def __init__(self, n_clusters, weights=None, alpha=1.0, **kwargs):
        super(ClusteringLayer, self).__init__(**kwargs)
        self.n_clusters = n_clusters
        self.alpha = alpha
        self.initial_weights = weights
        self.input_spec = InputSpec(ndim=2)

    def build(self, input_shape):
        self.input_spec = InputSpec(dtype=K.floatx(), shape=(None, input_shape[1]))
        self.clusters = self.add_weight((self.n_clusters, input_shape[1]), initializer='glorot_uniform', name='clusters')
        if self.initial_weights is not None:
            self.set_weights(self.initial_weights)
            del self.initial_weights
        self.built = True

    def call(self, inputs, **kwargs):
        q = (1.0 / (1.0 + (K.sum(K.square(K.expand_dims(inputs, axis=1) - self.clusters), axis=2) / self.alpha))) ** ((self.alpha + 1.0) / 2.0)
        return K.transpose(K.transpose(q) / K.sum(q, axis=1))

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.n_clusters)

    def get_config(self):
        config = list(({'n_clusters': self.n_clusters}).items())
        base_config = list((super(ClusteringLayer, self).get_config()).items())
        return dict(base_config + config)

def autoencoderConv2D_1(X, y):
    n_clusters = len(np.unique(y))
    input_shape=(32, 32, 3)
    filters=[32, 64, 128, 10]
    model = keras.models.Sequential()
    model.add(Conv2D(filters[0], 5, strides=2, padding='same', activation='relu', name='conv1', input_shape=input_shape))
    model.add(Conv2D(filters[1], 5, strides=2, padding='same', activation='relu', name='conv2'))
    model.add(Conv2D(filters[2], 3, strides=2, padding='same', activation='relu', name='conv3'))
    model.add(Flatten())
    model.add(Dense(units=filters[3], name='embedding'))
    kmeans = KMeans(n_clusters=n_clusters, n_init=20)
    kmeans.fit_predict(model.predict(X))
    model.add(ClusteringLayer(n_clusters, name='clustering', weights=[kmeans.cluster_centers_]))
    return model


from keras.datasets import cifar10
(x_train, y_train), (x_test, y_test) = cifar10.load_data()
# x_train = x_train.reshape(x_train.shape + (1,))
# x_test = x_test.reshape(x_test.shape + (1,))
x_train = np.divide(x_train, 255.)
x_test = np.divide(x_test, 255.)
y_test = y_test.flatten()

model = autoencoderConv2D_1(x_train, y_train)
model.compile(optimizer=SGD(0.01, 0.9), loss='kld')

loss = 0
index = 0
maxiter = 8000
batch_size = 256
update_interval_target = 140
index_array = np.arange(x_train.shape[0])

for i in range(int(maxiter)):
    if i >= 2000:
        break
    if i % update_interval_target == 0:
        q = model.predict(x_train, verbose=0)
        p = target_distribution(q)  # update the auxiliary target distribution p

    idx = index_array[index * batch_size: min((index+1) * batch_size, x_train.shape[0])]
    loss = model.train_on_batch(x=x_train[idx], y=p[idx])
    if (index + 1) * batch_size <= x_train.shape[0]:
        index = index + 1
    else:
        index = 0    

model.save('results/conv_DEC_model_final.h5')
q = model.predict(x_test, verbose=0)
p = target_distribution(q)

y_pred = q.argmax(1)

nmi = np.round(normalized_mutual_info_score(y_test, y_pred), 5)
ari = np.round(adjusted_rand_score(y_test, y_pred), 5)
loss = np.round(loss, 5)
print('nmi = %.5f, ari = %.5f' % (nmi, ari,))

# print(model.predict(np.array([x_test[0]]), verbose=0))
# print(model.predict(np.array([x_test[0]]), verbose=0).argmax(axis=-1))
# print(y_test[0])
# print(model.predict(np.array([x_test[0]]), verbose=0).shape)
