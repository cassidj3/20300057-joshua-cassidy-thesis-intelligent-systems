from genetic.genetics import Genetics
from genetic.population import Population
from genetic.member import Member
from genetic.utils import genes_utils
from genetic import genes
import copy

def generate_networks(base_dir, population_size = 5, generations = 3, crossover_rate = None, mutation_rate = None):
    if crossover_rate is not None:
        genes.CROSSOVER_RATE = crossover_rate
    
    if mutation_rate is not None:
        genes.MUTATION_RATE = mutation_rate

    generation = 0
    
    print("Generation", generation)
    genetics = Genetics()
    population = Population(population_size, generate_new_members=True, base_dir=base_dir)
    fittest = population.get_fittest_member()

    for i in range(0, generations):
        generation = i + 1
        parents_population = copy.deepcopy(population)
        population = genetics.evolve_population(population, base_dir=base_dir)
        print("Generation", generation)
        
        for member in population.members:
            member.evaluation()
            member.has_run = True
        
        if parents_population.members[i].fitness > population.members[i].fitness:
            population.members[i] = parents_population.members[i]

        if population.get_fittest_member().fitness > fittest.fitness:
            fittest = population.get_fittest_member()
        
    print(fittest)    
        
generate_networks("/Users/owner/Desktop/trinity-modules/thesis/thesis/extract_dataset/cifar10_dataset_split")
