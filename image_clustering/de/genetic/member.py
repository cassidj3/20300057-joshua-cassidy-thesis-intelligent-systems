import datetime
import math
import os

from . import cnn_template
from . import genes
from .utils import cluster_utils
from .utils import conv_layers_utils
from .utils import conv_nodes_utils
from .utils import dense_layers_utils
from .utils import dense_nodes_utils
from .utils import genes_utils
from .utils import image_utils
from .utils import kernel_utils
from .utils import network_dropouts_utils


class Member:
    def __init__(self, new_member, base_dir):
        self.base_dir = base_dir
        self.has_run = False

        self.loss = 0
        self.cluster_eval = 0
        self.compile_time = 0
        self.fitness = 0
        
        self.kernel_width = []
        self.kernel_height = []
        self.layers = []
        self.nodes = []
        self.dropouts = []
        
        self.dense_dropouts = []
        self.dense_layers = []
        self.dense_nodes = []
        
        self.n_clusters = []

        if new_member == True:
            self.generate()
        

    def generate(self):
        image_width, image_height = image_utils.dataset_dimensions(self.base_dir)

        conv_layers_utils.generate_layers(self.layers)
        dense_layers_utils.generate_dense_layers(self.dense_layers)
        
        layers_amount = genes_utils.genes_to_int(self.layers)
        dense_layers_amount = genes_utils.genes_to_int(self.dense_layers)
        
        conv_nodes_utils.generate_nodes(layers=self.layers, nodes=self.nodes, layers_amount=layers_amount)
        dense_nodes_utils.generate_dense_nodes(
            layers=self.dense_layers, 
            nodes=self.dense_nodes, 
            layers_amount=dense_layers_amount
        )

        network_dropouts_utils.generate_dropouts(self.dropouts, layers_amount)
        network_dropouts_utils.generate_dropouts(self.dense_dropouts, dense_layers_amount)
        cluster_utils.generate_clusters(self.n_clusters)
        
        self.kernel_width = kernel_utils.generate_kernel(layers_amount, image_width)
        self.kernel_height = kernel_utils.generate_kernel(layers_amount, image_height)
        
        self.evaluation()
        self.has_run = True

    def generate_network_code(self):
        layers = ""
        dense_layers = ""
        input_layer_code_template = "model.add(Conv2D({nodes}, ({kernel_width}, {kernel_height}), activation='relu', input_shape=input_shape))\n"
        layers_code_template = "model.add(Conv2D({nodes}, ({kernel_width}, {kernel_height}), activation='relu'))\n"
        dropout_code_template = "model.add(Dropout({weight}))\n    "
        dense_layer_code_template = "model.add(Dense({nodes}, activation='relu'))\n    "

        layers_amount = genes_utils.genes_to_int(self.layers)
        
        for i in range(0, layers_amount):
            if i != 0:
                layers += "    "
            nodes_amount = genes_utils.genes_to_int(self.nodes[i])
            kernel_width = genes_utils.genes_to_int(self.kernel_width[i])
            kernel_height = genes_utils.genes_to_int(self.kernel_height[i])
            
            if self.dropouts[i] != genes.DEFAULT_DROPOUT_LAYER:
                weight = genes_utils.genes_to_int(self.dropouts[i])/100
                layers += dropout_code_template.format(weight=1 if weight > 1 else weight)
            
            current_template = input_layer_code_template if i == 0 else layers_code_template

            layers += current_template.format(
                nodes=nodes_amount, 
                kernel_width=kernel_width, 
                kernel_height=kernel_height
            )


        # last_dense_layer_index = None
        dense_layers_amount = genes_utils.genes_to_int(self.dense_layers)

        for i in range(dense_layers_amount):
            layer_populated = False
            for node in self.dense_nodes[i]:
                if node == 1:
                    layer_populated = True
                    last_dense_layer_index = i
                    break

        # if last_dense_layer_index is not None:
        #     clusters_amount_int = genes_utils.genes_to_int(self.n_clusters)
        #     if genes_utils.genes_to_int(self.dense_nodes[last_dense_layer_index]) < clusters_amount_int:
        #         self.dense_nodes[last_dense_layer_index] = genes_utils.int_to_genes(
        #             round(clusters_amount_int / 2),
        #             genes.DENSE_NODES
        #         )
        
        for i in range(dense_layers_amount):
            nodes_amount = genes_utils.genes_to_int(self.dense_nodes[i])
            dense_layers += dense_layer_code_template.format(nodes=nodes_amount)
            if self.dense_dropouts[i] != genes.DEFAULT_DROPOUT_LAYER:
                weight = genes_utils.genes_to_int(self.dense_dropouts[i])/100
                dense_layers += dropout_code_template.format(weight=1 if weight > 1 else weight)
        
        

        network_code = cnn_template.code.format(
            layers=layers, 
            dense_layers=dense_layers,
            n_clusters = genes_utils.genes_to_int(self.n_clusters)
        )
        
        return network_code

    def evaluation(self):
        if self.has_run:
            return self.fitness
        
        network_code = self.generate_network_code()
        start_compile_time = datetime.datetime.now()

        execute_net_code = """create_neural_net(
            base_dir="{base_dir}", 
            train_dir="{train_dir}", 
            test_dir="{test_dir}",
        )""".format(
            base_dir=self.base_dir, 
            train_dir=os.path.join(self.base_dir, 'train'), 
            test_dir=os.path.join(self.base_dir, 'test'),
        )
        
        context = {}
        exec(network_code + "\ntest_loss, test_cluster_eval = " + execute_net_code, context)
        end_compile_time = datetime.datetime.now()

        self.loss = context['test_loss']
        self.cluster_eval = context['test_cluster_eval']
        self.compile_time = end_compile_time - start_compile_time
        self.fitness = math.exp(math.exp(math.exp(self.cluster_eval - self.loss)))

        return self.fitness
    
    def __str__(self):
        return """Member(
                    kernel_width={kernel_width},
                    kernel_height={kernel_height},
                    layers={layers} 
                    nodes={nodes}, 
                    dropouts={dropouts},
                    dense_layers={dense_layers},
                    dense_nodes={dense_nodes},
                    dense_dropouts={dense_dropouts},
                    cluster_eval={cluster_eval},
                    fitness={fitness}, 
                    loss={loss}, 
                    compile_time={compile_time},
                    clusters={clusters},
                )""".format(
                    kernel_width=str(self.kernel_width),
                    kernel_height=str(self.kernel_height),
                    layers=str(self.layers),
                    nodes=str(self.nodes),
                    dropouts=str(self.dropouts),
                    dense_layers=str(self.dense_layers),
                    dense_nodes=str(self.dense_nodes),
                    dense_dropouts=str(self.dense_dropouts),
                    cluster_eval=str(self.cluster_eval),
                    fitness=str(self.fitness),
                    loss=str(self.loss),
                    compile_time=str(self.compile_time),
                    clusters=str(self.n_clusters),
                )
