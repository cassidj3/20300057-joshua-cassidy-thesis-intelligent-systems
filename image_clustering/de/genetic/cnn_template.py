code = """
import matplotlib
matplotlib.use("TkAgg")
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os

from keras.layers import Dropout
from keras.layers import Conv2D, MaxPooling2D
import keras
import matplotlib.image as mpimg
from keras.utils import plot_model
from validclust import dunn
from sklearn.metrics import pairwise_distances

import keras.backend as K
from keras.engine.topology import Layer, InputSpec
from keras.optimizers import SGD
from sklearn.cluster import KMeans
from sklearn.metrics import calinski_harabasz_score
from sklearn.metrics import silhouette_score
from sklearn.metrics import adjusted_rand_score
from sklearn.metrics import davies_bouldin_score
from sklearn.metrics import normalized_mutual_info_score

from keras import layers
from keras.layers import Input, Dense, MaxPooling2D, UpSampling2D, Flatten, Reshape, Conv2DTranspose
import numpy as np
import keras

np.random.seed(0) 
tf.set_random_seed(0)

def target_distribution(q):
    weight = q ** 2 / q.sum(0)
    return (weight.T / weight.sum(1)).T 

class ClusteringLayer(Layer):
    def __init__(self, n_clusters, weights=None, alpha=1.0, **kwargs):
        super(ClusteringLayer, self).__init__(**kwargs)
        self.n_clusters = n_clusters
        self.alpha = alpha
        self.initial_weights = weights
        self.input_spec = InputSpec(ndim=2)

    def build(self, input_shape):
        self.input_spec = InputSpec(dtype=K.floatx(), shape=(None, input_shape[1]))
        self.clusters = self.add_weight((self.n_clusters, input_shape[1]), initializer='glorot_uniform', name='clusters')
        if self.initial_weights is not None:
            self.set_weights(self.initial_weights)
            del self.initial_weights
        self.built = True

    def call(self, inputs, **kwargs):
        q = (1.0 / (1.0 + (K.sum(K.square(K.expand_dims(inputs, axis=1) - self.clusters), axis=2) / self.alpha))) ** ((self.alpha + 1.0) / 2.0)
        return K.transpose(K.transpose(q) / K.sum(q, axis=1))

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.n_clusters)

    def get_config(self):
        config = [('n_clusters', self.n_clusters)]
        base_config = list((super(ClusteringLayer, self).get_config()).items())
        return dict(base_config + config)

def autoencoderConv2D_1(X, input_shape):
    n_clusters = {n_clusters}
    model = keras.models.Sequential()
    {layers}
    model.add(Flatten())
    {dense_layers}
    model.add(Dense(units=n_clusters))
    kmeans = KMeans(n_clusters=n_clusters, n_init=20)
    kmeans.fit_predict(model.predict(X))
    model.add(ClusteringLayer(n_clusters, weights=[kmeans.cluster_centers_]))
    return model


def get_folder_contents(path):
    folder_contents = [item for item in os.listdir(path) if not item.startswith('.')]
    return folder_contents

def extract_images(images_dir, image_shape):
    image_data = []
    i = 0
    for (dirpath, dirnames, filenames) in os.walk(images_dir):
        for filename in filenames:
            try:
                if i > 1000:
                    break
                i+=1
                image = mpimg.imread(os.path.join(dirpath, filename))
                image_data.append(image)
            except:
                pass
            
    image_data = np.array(image_data).reshape((len(image_data),) + (image_shape))
    image_data = np.divide(image_data, 255.)
    return image_data

def create_neural_net(base_dir, train_dir, test_dir):
    data_folder_name = get_folder_contents(train_dir)[0]
    image_name = get_folder_contents(os.path.join(train_dir, data_folder_name) )[0]
    image_shape = mpimg.imread(os.path.join(train_dir, data_folder_name, image_name)).shape

    # x_train = extract_images(train_dir, image_shape)
    x_test = extract_images(test_dir, image_shape)
    x_train = x_test

    model = autoencoderConv2D_1(x_train, image_shape)
    model.compile(optimizer=SGD(0.01, 0.9), loss='kld')
    model.summary()

    loss = 0
    index = 0
    maxiter = 1
    batch_size = 256
    update_interval_target = 140
    index_array = np.arange(x_train.shape[0])
    # 20000
    for i in range(int(maxiter)):
        if i % update_interval_target == 0:

            q = model.predict(x_train, verbose=0)
            p = target_distribution(q)  # update the auxiliary target distribution p

        idx = index_array[index * batch_size: min((index+1) * batch_size, x_train.shape[0])]
        loss = model.train_on_batch(x=x_train[idx], y=p[idx])
        if (index + 1) * batch_size <= x_train.shape[0]:
            index = index + 1
        else:
            index = 0    

    q = model.predict(x_test, verbose=0)
    p = target_distribution(q)

    y_pred = q.argmax(1)
    dist = pairwise_distances(x_test.reshape(len(x_test),-1))
    try:
        silhouette = silhouette_score(x_test.reshape(len(x_test), -1), y_pred, metric='euclidean')
    except:
        silhouette = -1
    
    print("silhouette =", silhouette)
    print("loss =", loss)
    loss = np.round(loss, 5)

    keras.backend.clear_session()
    return (loss, silhouette)
    
"""
