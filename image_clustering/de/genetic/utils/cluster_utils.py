import random
from .. import genes


def crossover_clusters(child_clusters, parent1_clusters, parent2_clusters):
    for i in range(genes.CLUSTERS):
        inherit_parent_1_gene = random.uniform(0, 1) < genes.CROSSOVER_RATE
        inherited_gene = parent1_clusters[i] if inherit_parent_1_gene else parent2_clusters[i]
        child_clusters.append(inherited_gene)
    
    if int('0b' +''.join(str(i) for i in child_clusters), 2) < 2:
        child_clusters[-1] = 0
        child_clusters[-2] = 1

def generate_clusters(child_clusters):
    [child_clusters.append(random.randint(0, 1)) for _ in range(genes.CLUSTERS)]
    
    if int('0b' +''.join(str(i) for i in child_clusters), 2) < 2:
        child_clusters[-1] = 0
        child_clusters[-2] = 1


