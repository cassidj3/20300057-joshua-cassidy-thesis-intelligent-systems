from .. import genes as genes
import random
from . import genes_utils, mutation_utils

def crossover_dense_layers(child_layers, parent1_layers, parent2_layers):
    for i in range(genes.DENSE_LAYERS):
        inherit_parent_1_gene = random.uniform(0, 1) < genes.CROSSOVER_RATE
        parent1_gene = parent1_layers[i]
        parent2_gene = parent2_layers[i]
        inherited_gene = parent1_gene if inherit_parent_1_gene else parent2_gene
        child_layers.append(inherited_gene)

def generate_dense_layers(layers):
    [layers.append(random.randint(0, 1)) for _ in range(genes.DENSE_LAYERS)]

def mutate_dense_layers(alpha_layers, beta_layers, gamma_layers):
    child_layers = mutation_utils.calculate_parameter_mutation(alpha_layers, beta_layers, gamma_layers, genes_size = genes.DENSE_LAYERS)
    return child_layers

