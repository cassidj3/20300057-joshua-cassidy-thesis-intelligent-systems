from .. import genes as genes
import random
from . import genes_utils


def calculate_parameter_mutation(alpha_parameter, beta_parameter, gamma_parameter, genes_size, min_parameter = 1):
    mutation_rate = 1
    alpha_parameter = genes_utils.genes_to_int(alpha_parameter)
    beta_parameter = genes_utils.genes_to_int(beta_parameter)
    gamma_parameter = genes_utils.genes_to_int(gamma_parameter)
    mutated_parameter = alpha_parameter + (mutation_rate * (beta_parameter - gamma_parameter))

    if mutated_parameter <= min_parameter:
        mutated_parameter = min_parameter

    return genes_utils.int_to_genes(mutated_parameter, genes_size)

