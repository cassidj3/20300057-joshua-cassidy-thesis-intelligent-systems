from .. import genes as genes
import random
from . import genes_utils, mutation_utils

def crossover_kernel(child, parent1_kernel, parent2_kernel, layers_amount, downsized_image_size):
    child_kernel = []
    for kernel in range(layers_amount):
        inherited_kernel = []
        for i in range(genes.WIDTH_KERNEL):
            inherit_parent_1_gene = random.uniform(0, 1) < genes.CROSSOVER_RATE
            parent1_gene = parent1_kernel[kernel % len(parent1_kernel)]
            parent2_gene = parent2_kernel[kernel % len(parent2_kernel)]
            inherited_gene = parent1_gene if inherit_parent_1_gene else parent2_gene
            inherited_kernel.append(inherited_gene[i])
        
        validate_kernel(inherited_kernel)
        kernel_size = genes_utils.genes_to_int(inherited_kernel)

        if downsized_image_size - (kernel_size-1) >= 1:
            downsized_image_size = downsized_image_size - (kernel_size-1)

        if downsized_image_size - (kernel_size-1) <= 1:
            random_kernel = random.randint(1, max([downsized_image_size, 1]))
            downsized_image_size = downsized_image_size - (random_kernel-1)
            inherited_kernel = genes_utils.int_to_genes(random_kernel, genes.WIDTH_KERNEL)
        child_kernel.append(inherited_kernel)
    return child_kernel

def mutate_kernel(alpha_kernels, beta_kernels, gamma_kernels, downsized_image_size):
    mutated_kernels = []
    for kernel in range(max([len(alpha_kernels), len(beta_kernels), len(gamma_kernels)])):
        alpha = alpha_kernels[kernel%len(alpha_kernels)]
        beta = beta_kernels[kernel%len(beta_kernels)]
        gamma = gamma_kernels[kernel%len(gamma_kernels)]
        
        mutated_kernel = mutation_utils.calculate_parameter_mutation(alpha, beta, gamma, genes_size=genes.WIDTH_KERNEL)
        mutated_kernel = validate_kernel(mutated_kernel)
        kernel_size = genes_utils.genes_to_int(mutated_kernel)

        if downsized_image_size - (kernel_size-1) >= 1:
            downsized_image_size = downsized_image_size - (kernel_size-1)

        if downsized_image_size - (kernel_size-1) <= 1:
            random_kernel = random.randint(1, max([downsized_image_size, 1]))
            downsized_image_size = downsized_image_size - (random_kernel-1)
            mutated_kernel = genes_utils.int_to_genes(random_kernel, genes.WIDTH_KERNEL)
        mutated_kernels.append(mutated_kernel)
    return mutated_kernels


def validate_kernel(kernel):
    invalid_kernel = True
    for k in kernel:
        if k == 1:
            invalid_kernel = False
            break

    if invalid_kernel:
        kernel[len(kernel) - 1] = 1
    return kernel

def generate_kernel(layers_amount, downsized_image_size):
    kernel = []
    for i in range(layers_amount):
        generated_kernel = [random.randint(0, 1) for _ in range(genes.WIDTH_KERNEL)]

        validate_kernel(generated_kernel)
        kernel_size = genes_utils.genes_to_int(generated_kernel)

        if downsized_image_size - (kernel_size-1) >= 1:
            downsized_image_size = downsized_image_size - (kernel_size-1)

        if downsized_image_size - (kernel_size-1) <= 1:
            random_kernel = random.randint(1, max([downsized_image_size, 1]))
            downsized_image_size = downsized_image_size - (random_kernel-1)
            generated_kernel = genes_utils.int_to_genes(random_kernel, genes.WIDTH_KERNEL)

        kernel.append(generated_kernel)
    return kernel


