from .. import genes as genes
import random
from . import genes_utils, mutation_utils, dense_layers_utils, network_nodes_utils

# def crossover_dense_nodes(child_layers, child_nodes, parent1_nodes, parent2_nodes, layers_amount):
#     if layers_amount == 0:
#         temp_nodes = [0 for _ in range(genes.DENSE_NODES)]
#         child_nodes.append(temp_nodes)
#     else:
#         for layer in range(layers_amount):
#             inherited_nodes = []

#             parent1_nodes_temp = parent1_nodes[layer % len(parent1_nodes)]
#             parent2_nodes_temp = parent2_nodes[layer % len(parent2_nodes)]
#             for i in range(genes.DENSE_NODES):
#                 inherit_parent_1_gene = random.uniform(0, 1) < genes.CROSSOVER_RATE
#                 parent1_gene = parent1_nodes_temp[i % len(parent1_nodes_temp)]
#                 parent2_gene = parent2_nodes_temp[i % len(parent2_nodes_temp)]
#                 inherited_gene = parent1_gene if inherit_parent_1_gene else parent2_gene
#                 inherited_nodes.append(inherited_gene)

#             child_nodes.append(inherited_nodes)

#         child_nodes = validate_dense_nodes(layers=child_layers, nodes=child_nodes)


def crossover_dense_nodes(child_layers, child_nodes, parent1_nodes, parent2_nodes, layers_amount):
    if layers_amount == 0:
        child_nodes.append([0 for _ in range(genes.DENSE_NODES)])
    else:
        for layer in range(layers_amount):
            temp_nodes = []

            parent1_nodes_temp = parent1_nodes[layer % len(parent1_nodes)]
            parent2_nodes_temp = parent2_nodes[layer % len(parent2_nodes)]
            for i in range(genes.DENSE_NODES):
                if random.uniform(0, 1) < genes.CROSSOVER_RATE:
                    temp_nodes.append(parent1_nodes_temp[i % len(parent1_nodes_temp)])
                else:
                    temp_nodes.append(parent2_nodes_temp[i % len(parent2_nodes_temp)])
            child_nodes.append(temp_nodes)
        
        child_nodes = validate_dense_nodes(layers=child_layers, nodes=child_nodes)


def validate_dense_nodes(layers, nodes):
    mutated_layers = network_nodes_utils.validate_layers(layers)

    for node in nodes:
        for i in node:
            invalid_nodes_value = True
            if i == 1:
                invalid_nodes_value = False
                break
        if invalid_nodes_value:
            node[len(node) - 1] = 1
        

    return nodes


def generate_dense_nodes(layers, nodes, layers_amount):
    if layers_amount == 0:
        nodes.append([0 for _ in range(genes.DENSE_NODES)])
    else:
        for i in range(layers_amount):
            nodes.append([random.randint(0, 1) for _ in range(genes.DENSE_NODES)])
        nodes = validate_dense_nodes(layers=layers, nodes=nodes)
        
def mutate_nodes(alpha_nodes, beta_nodes, gamma_nodes, mutated_layers, genes_size):
    mutated_nodes = []
    for node in range(max([len(alpha_nodes), len(beta_nodes), len(gamma_nodes)])):
        alpha = alpha_nodes[node % len(alpha_nodes)]
        beta = beta_nodes[node % len(beta_nodes)]
        gamma = gamma_nodes[node % len(gamma_nodes)]
        
        mutated_node = mutation_utils.calculate_parameter_mutation(alpha, beta, gamma, genes_size)
        mutated_nodes.append(mutated_node)
    
    return validate_dense_nodes(layers=mutated_layers, nodes=mutated_nodes)
    


