from .. import genes as genes
import random
from . import genes_utils, mutation_utils


def mutate_dropouts(alpha_dropouts, beta_dropouts, gamma_dropouts):
    mutated_dropouts = []
    for dropout in range(max([len(alpha_dropouts), len(beta_dropouts), len(gamma_dropouts)])):
        alpha = alpha_dropouts[dropout%len(alpha_dropouts)]
        beta = beta_dropouts[dropout%len(beta_dropouts)]
        gamma = gamma_dropouts[dropout%len(gamma_dropouts)]
        
        mutated_dropout = mutation_utils.calculate_parameter_mutation(alpha, beta, gamma, genes_size = len(genes.DEFAULT_DROPOUT_LAYER))
        if genes_utils.genes_to_int(mutated_dropout) > 100:
            mutated_dropout = genes_utils.int_to_genes(100, len(genes.DEFAULT_DROPOUT_LAYER))
        mutated_dropouts.append(mutated_dropout)
        
    return mutated_dropouts


def generate_dropouts(dropouts, layers_amount):
    if layers_amount  == 0:
        dropouts.append(genes.DEFAULT_DROPOUT_LAYER)
    else:
        for _ in range(layers_amount):
            dropouts.append(genes.DEFAULT_DROPOUT_LAYER)

        dropout_amount = random.randint(0, layers_amount)
        layer_indexes = list(range(0, dropout_amount))
        for _ in range(dropout_amount):
            dropout_index = random.choice(layer_indexes)
            layer_indexes.remove(dropout_index)

            dropout_weight = genes_utils.int_to_genes(round(random.uniform(0,1)*100), len(genes.DEFAULT_DROPOUT_LAYER))
            dropout_remaining = [0 for i in range(7 - len(dropout_weight))]
            dropouts[dropout_index] = dropout_remaining + dropout_weight

