import random
from . import genes_utils
from .. import genes

def crossover_dropouts(child, parent1, parent2, layers_amount):
    for layer in range(layers_amount):
        
        parent1_dropouts = parent1.dropouts[layer % len(parent1.dropouts)]
        parent2_dropouts = parent2.dropouts[layer % len(parent2.dropouts)]
        inherited_dropout_layer = []
        for dropout_gene in range(7):
            inherit_parent_1_gene = random.uniform(0, 1) < genes.CROSSOVER_RATE
            parent1_gene = parent1_dropouts[dropout_gene]
            parent2_gene = parent2_dropouts[dropout_gene]
            inherited_gene = parent1_gene if inherit_parent_1_gene else parent2_gene
            inherited_dropout_layer.append(inherited_gene)

        
        if genes_utils.genes_to_int(inherited_dropout_layer) > 100:
            inherited_dropout_layer = genes_utils.int_to_genes(100, len(genes.DEFAULT_DROPOUT_LAYER))
        child.dropouts.append(inherited_dropout_layer)
