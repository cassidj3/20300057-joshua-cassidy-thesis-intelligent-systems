def validate_layers(layers):
    invalid_layer = True
    for layer_gene in layers:
        if layer_gene == 1:
            invalid_layer = False
            break

    if invalid_layer:
        layers[len(layers) - 1] = 1
    return layers