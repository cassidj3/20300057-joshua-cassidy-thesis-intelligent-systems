import os
import matplotlib.image as mpimg

def get_folder_contents(path):
    folder_contents = [item for item in os.listdir(path) if not item.startswith('.')]
    return folder_contents

def dataset_dimensions(base_dir):
    classification_folder_name = get_folder_contents(os.path.join(base_dir, 'train'))[0]
    images_path = os.path.join(base_dir, 'train', classification_folder_name)
    image_shape = mpimg.imread(os.path.join(images_path, get_folder_contents(images_path)[0])).shape
    return (image_shape[0], image_shape[1])
