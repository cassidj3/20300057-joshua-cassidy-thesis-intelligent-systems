from .member import Member

class Population:

    def __init__(self, size, generate_new_members, base_dir):
        self.size = size
        self.members = []
        for _ in range(size):
            self.members.append(Member(new_member=generate_new_members, base_dir=base_dir))

    def get_fittest_member(self):
        fittest_member = self.members[0]
        for member in self.members:
            if member.fitness > fittest_member.fitness:
                fittest_member = member
        return fittest_member
