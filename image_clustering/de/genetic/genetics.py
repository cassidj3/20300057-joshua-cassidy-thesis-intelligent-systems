import copy
import random

from . import genes
from .member import Member
from .population import Population

from .utils import conv_dropouts_utils
from .utils import conv_nodes_utils
from .utils import conv_layers_utils
from .utils import cluster_utils
from .utils import dense_dropouts_utils
from .utils import dense_nodes_utils
from .utils import dense_layers_utils
from .utils import genes_utils
from .utils import image_utils
from .utils import kernel_utils
from .utils import mutation_utils
from .utils import network_dropouts_utils


class Genetics:

    def __init__(self, base_dir=None):
        self.base_dir = base_dir

    def evolve_population(self, population, base_dir):
        self.base_dir = base_dir

        next_generation = Population(
            population.size, 
            generate_new_members=False, 
            base_dir=self.base_dir, 
        )

        for i in range(population.size):
            population_copy = copy.deepcopy(population.members)
            del population_copy[i]
            alpha = random.choice(population_copy)
            population_copy.remove(alpha)
            beta = random.choice(population_copy)
            population_copy.remove(beta)
            gamma = random.choice(population_copy)
            population_copy.remove(gamma)
            
            parent1 = self.mutate(alpha, beta, gamma)
            parent2 = population.members[i]
            next_generation.members[i] = self.crossover(parent1, parent2)

        return next_generation

    def crossover(self, parent1, parent2):
        child = Member(new_member=False, base_dir=self.base_dir)
        conv_layers_utils.crossover_layers(
            child_layers=child.layers, 
            parent1_layers=parent1.layers, 
            parent2_layers=parent2.layers
        )
        
        dense_layers_utils.crossover_dense_layers(
            child_layers=child.dense_layers, 
            parent1_layers=parent1.dense_layers, 
            parent2_layers=parent2.dense_layers
        )

        layers_amount = genes_utils.genes_to_int(child.layers)
        dense_layers_amount = genes_utils.genes_to_int(child.dense_layers)

        conv_nodes_utils.crossover_nodes(
            child_nodes=child.nodes, 
            child_layers=child.layers, 
            parent1_nodes=parent1.nodes, 
            parent2_nodes=parent2.nodes, 
            layers_amount=layers_amount
        )

        dense_nodes_utils.crossover_dense_nodes(
            child_nodes=child.dense_nodes, 
            child_layers=child.dense_layers, 
            parent1_nodes=parent1.dense_nodes, 
            parent2_nodes=parent2.dense_nodes, 
            layers_amount=dense_layers_amount
        )

        conv_dropouts_utils.crossover_dropouts(child, parent1, parent2, layers_amount)
        dense_dropouts_utils.crossover_dense_dropouts(child, parent1, parent2, dense_layers_amount)

        downsized_image_width, downsized_image_height = image_utils.dataset_dimensions(self.base_dir)
        child.kernel_width = kernel_utils.crossover_kernel(
            child=child, 
            parent1_kernel=parent1.kernel_width,
            parent2_kernel=parent2.kernel_width, 
            layers_amount=layers_amount,
            downsized_image_size=downsized_image_width
        )
        child.kernel_height = kernel_utils.crossover_kernel(
            child=child, 
            parent1_kernel=parent1.kernel_height,
            parent2_kernel=parent2.kernel_height, 
            layers_amount=layers_amount,
            downsized_image_size=downsized_image_height
        )
        
        cluster_utils.crossover_clusters(
            child_clusters=child.n_clusters, 
            parent1_clusters=parent1.n_clusters, 
            parent2_clusters=parent2.n_clusters
        )

        return child


    def mutate(self, alpha, beta, gamma):
        new_member = Member(new_member=False, base_dir=self.base_dir)
        
        new_member.layers = conv_layers_utils.mutate_layers(
            alpha.layers, 
            beta.layers, 
            gamma.layers
        )
        
        new_member.dense_layers = dense_layers_utils.mutate_dense_layers(
            alpha.dense_layers, 
            beta.dense_layers, 
            gamma.dense_layers
        )

        new_member.nodes = conv_nodes_utils.mutate_nodes(
            alpha.nodes, 
            beta.nodes, 
            gamma.nodes, 
            new_member.layers, 
            genes.NODES
        )
        new_member.dense_nodes = dense_nodes_utils.mutate_nodes(
            alpha.dense_nodes, 
            beta.dense_nodes, 
            gamma.dense_nodes, 
            new_member.dense_layers, 
            genes.DENSE_LAYERS
        )
        
        new_member.dropouts = network_dropouts_utils.mutate_dropouts(
            alpha.dropouts, 
            beta.dropouts, 
            gamma.dropouts
        )

        new_member.dense_dropouts = network_dropouts_utils.mutate_dropouts(
            alpha.dense_dropouts, 
            beta.dense_dropouts, 
            gamma.dense_dropouts
        )
        
        downsized_image_width, downsized_image_height = image_utils.dataset_dimensions(self.base_dir)
        new_member.kernel_width = kernel_utils.mutate_kernel(
            alpha.kernel_width, 
            beta.kernel_width, 
            gamma.kernel_width, 
            downsized_image_width
        )
        new_member.kernel_height = kernel_utils.mutate_kernel(
            alpha.kernel_height, 
            beta.kernel_height, 
            gamma.kernel_height, 
            downsized_image_height
        )
        new_member.n_clusters = mutation_utils.calculate_parameter_mutation(
            alpha.n_clusters, 
            beta.n_clusters, 
            gamma.n_clusters, 
            genes.CLUSTERS, 
            min_parameter = 2
        )
        
        return new_member

