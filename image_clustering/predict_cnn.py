import keras.backend as K
from keras.engine.topology import Layer, InputSpec
from keras.layers import Dense, Input
from keras.optimizers import SGD
from sklearn.cluster import KMeans
from keras import layers
from keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D, Flatten, Reshape, Conv2DTranspose
from keras.models import Model
import numpy as np
import keras
import tensorflow as tf


class ClusteringLayer(Layer):
    def __init__(self, n_clusters, weights=None, alpha=1.0, **kwargs):
        super(ClusteringLayer, self).__init__(**kwargs)
        self.n_clusters = n_clusters
        self.alpha = alpha
        self.initial_weights = weights
        self.input_spec = InputSpec(ndim=2)

    def build(self, input_shape):
        self.input_spec = InputSpec(dtype=K.floatx(), shape=(None, input_shape[1]))
        self.clusters = self.add_weight((self.n_clusters, input_shape[1]), initializer='glorot_uniform', name='clusters')
        if self.initial_weights is not None:
            self.set_weights(self.initial_weights)
            del self.initial_weights
        self.built = True

    def call(self, inputs, **kwargs):
        q = (1.0 / (1.0 + (K.sum(K.square(K.expand_dims(inputs, axis=1) - self.clusters), axis=2) / self.alpha))) ** ((self.alpha + 1.0) / 2.0)
        return K.transpose(K.transpose(q) / K.sum(q, axis=1))

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.n_clusters)

    def get_config(self):
        config = list(({'n_clusters': self.n_clusters}).items())
        base_config = list((super(ClusteringLayer, self).get_config()).items())
        return dict(base_config + config)

model = keras.models.load_model('results/conv_DEC_model_final.h5', custom_objects={'ClusteringLayer': ClusteringLayer})

from keras.datasets import cifar10
(x_train, y_train), (x_test, y_test) = cifar10.load_data()
# x_train = x_train.reshape(xpest.shape + (1,))
y_test = y_test.flatten()
y_train = y_train.flatten()
# print(y_test.reshape(1))
x_test = np.divide(x_test, 255.)


from sklearn.metrics import normalized_mutual_info_score, adjusted_rand_score, davies_bouldin_score, silhouette_score

def target_distribution(q):
    weight = q ** 2 / q.sum(0)
    return (weight.T / weight.sum(1)).T 

q = model.predict(x_test, verbose=0)
p = target_distribution(q)

y_pred = q.argmax(1)

nmi = np.round(normalized_mutual_info_score(y_test, y_pred), 5)
ari = np.round(adjusted_rand_score(y_test, y_pred), 5)
print('nmi = %.5f, ari = %.5f' % (nmi, ari,))

import os
import shutil

try:
    shutil.rmtree("predictions")
except:
    pass

os.mkdir("predictions")
for i in np.unique(y_test):
    os.mkdir("predictions/" + str(i))


from PIL import Image, ImageOps

CIFAR10_LABELS = {
    0: 'airplane', 
    1: 'automobile', 
    2: 'bird', 
    3: 'cat',
    4: 'deer', 
    5: 'dog', 
    6: 'frog', 
    7: 'horse', 
    8: 'ship', 
    9: 'truck'
}

labelling = {}


count = 0
for i in x_test:
    label = model.predict(np.array([i]), verbose=0).argmax(axis=-1)
    
    if label[0] not in labelling:
        labelling[label[0]] = {

        }

    if CIFAR10_LABELS[y_test[count]] not in labelling[label[0]]:
        labelling[label[0]][CIFAR10_LABELS[y_test[count]]] = 0

    labelling[label[0]][CIFAR10_LABELS[y_test[count]]] += 1

    filename = '{image_number}.jpg'.format(image_number=str(count) + "_" + str(CIFAR10_LABELS[y_test[count]]) )
    file_url = os.path.join("predictions", str(label[0]), filename)
    
    ImageOps.invert(Image.fromarray(np.multiply(i, 255).astype('uint8'))).save(file_url)
    if count > 10:
        break

    count+=1


print(labelling)