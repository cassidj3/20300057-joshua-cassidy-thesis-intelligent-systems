MSc Computer Science - Intelligent Systems
Name: Joshua Cassidy
Student Number: 20300057

Install the Projects dependancies
The following steps detail how to install the projects python dependencies:
1. Use the cd command in order to navigate to the directory where project has been downloaded
2. Run the "pip3 install -r requirements.txt" command to install the project's python dependencies (Note: ensure that you are connected to the internet)

Running the extract dataset component
The following steps detail how to run the extract dataset component:
1. Use the cd command in order to navigate to the extract_dataset component of the downloaded project
2. Run the "python3 extract_cifar10.py" command in order to extract the full CIFAR10 dataset
3. Run the "python3 extract_split_cifar10.py" command in order to extract the CIFAR10 dataset with 50,000 training records and 10,000 test records