import os
from PIL import Image, ImageOps

def write_images(x, y, cifar10_labels, output_dir, image_number):
    for i in range(0, len(y)):
        label_name = cifar10_labels[y[i][0]]
        filename = '{image_number}.jpg'.format(image_number=i+image_number)
        file_url = os.path.join(output_dir, label_name, filename)
        ImageOps.invert(Image.fromarray(x[i].astype('uint8'))).save(file_url)

def demo_write_images(x, y, cifar10_labels, output_dir, image_number):
    for i in range(0, 30):
        label_name = cifar10_labels[y[i][0]]
        filename = '{image_number}.jpg'.format(image_number=i+image_number)
        file_url = os.path.join(output_dir, label_name, filename)
        ImageOps.invert(Image.fromarray(x[i].astype('uint8'))).save(file_url)