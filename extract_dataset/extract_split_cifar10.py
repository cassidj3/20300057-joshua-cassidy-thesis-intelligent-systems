import os, shutil
import numpy as np
from keras.datasets import cifar10
import extract_data_utils
import dataset_labels

(x_train, y_train), (x_test, y_test) = cifar10.load_data()
output_dir = 'cifar10_dataset_split'

if os.path.exists(output_dir):
    shutil.rmtree(output_dir)

train_data = os.path.join(output_dir, "train")
test_data = os.path.join(output_dir, "test")
if not os.path.exists(output_dir):
    os.mkdir(output_dir)

for directory in [train_data, test_data]:
    if not os.path.exists(directory):
        os.mkdir(directory)
    for i in range(len(x_train)):
        label_name = dataset_labels.CIFAR10_LABELS[y_train[i][0]]
        folder_name = os.path.join(directory, label_name)
        if not os.path.exists(folder_name):
            os.mkdir(folder_name)

extract_data_utils.write_images(x_train, y_train, dataset_labels.CIFAR10_LABELS, train_data, 0)
extract_data_utils.write_images(x_test, y_test, dataset_labels.CIFAR10_LABELS, test_data, len(x_train))

